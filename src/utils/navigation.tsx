import React from "react";
import App from "../App";

export const routeAll = (token: any) => {
  return [
    {
      path: "/",
      private: true,
      component: <App token={token} />,
    },
  ];
};
