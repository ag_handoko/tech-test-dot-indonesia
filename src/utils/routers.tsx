/* eslint-disable no-undef */
import React from "react";
import { connect } from "react-redux";
import { Route, Routes, BrowserRouter } from "react-router-dom";

import { routeAll } from "./navigation";
import { PrivateRouter, ReverseRouter } from "../middleware/credentials";
import { Login } from "../component/Auth/Login";
import { Register } from "../component/Auth/Register";
import { ForgetPassword } from "../component/Auth/ForgetPassword";

const _Routers = () => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role");

  const setRoutes = () => {
    switch (role) {
      case "admin":
        return routeAll(token);
      default:
        return routeAll(token);
    }
  };

  return (
    <BrowserRouter>
      <Routes>
        {setRoutes()?.map((items, index) => (
          <Route
            key={index}
            path={items.path}
            element={<PrivateRouter>{items.component}</PrivateRouter>}
          />
        ))}
        <Route
          path="/login"
          element={
            <ReverseRouter>
              <Login />
            </ReverseRouter>
          }
        />
        <Route
          path="/register"
          element={
            <ReverseRouter>
              <Register />
            </ReverseRouter>
          }
        />
        <Route
          path="/forgot-password"
          element={
            <ReverseRouter>
              <ForgetPassword />
            </ReverseRouter>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};

const mapStateToProps = ({ reducerAuth }: any) => {
  return { ...reducerAuth };
};

export const Routers = connect(mapStateToProps)(_Routers);

_Routers.propTypes = {};
