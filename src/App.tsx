import Home from './component/Home';
import './App.css';
import 'materialize-css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'react-spring-bottom-sheet/dist/style.css';

type props = {
	token?: object | {};
};

const App = ({ token }: props) => {
	return <Home token={token} />;
};

export default App;
