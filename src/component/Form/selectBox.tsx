import React from 'react';
import './style.css';

type propTypes = {
	id?: string;
	name?: string;
	value?: string | number;
	placeholder?: string;
	onChange?: (e: any) => void;
};

const SelectField = ({ id, name, value, onChange }: propTypes) => {
	return (
		<div className='form-group'>
			<select id={id} onChange={onChange} name={name} value={value}>
				<option>- Pilih Status -</option>
				<option value={0}>Belum Selesai</option>
				<option value={1}>Sudah Selesai</option>
			</select>
		</div>
	);
};

export default SelectField;
