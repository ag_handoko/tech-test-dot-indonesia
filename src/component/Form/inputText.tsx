import React from "react";
import "./style.css";

type propTypes = {
  id?: string;
  name?: string;
  value?: string;
  type?: string;
  placeholder?: string;
  className?: string;
  onChange?: (e: any) => void;
};

const TextField = ({
  id,
  name,
  value,
  type = "text",
  placeholder = "",
  onChange,
  className = "",
}: propTypes) => {
  return (
    <div className={`form-group ${className}`}>
      <input
        type={type}
        id={id}
        name={name}
        value={value}
        placeholder={placeholder}
        onChange={(e) => onChange?.(e)}
      />
    </div>
  );
};

export default TextField;
