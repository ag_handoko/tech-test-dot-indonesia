import React from "react";
import { connect } from "react-redux";

import TextField from "../../Form/inputText";
import "./style.css";
import { Link } from "react-router-dom";

type formTypes = {
  username?: string;
  password?: string;
};

const FORM_INITIAL: formTypes = {
  username: "",
  password: "",
};

const _Login = ({ loading, dispatch, users }: any) => {
  const [formData, setFormData] = React.useState(FORM_INITIAL);

  React.useEffect(() => {
    dispatch({
      type: "auth/get_state",
    });

    return;
  }, [dispatch]);

  const onChange = (event: any) => {
    const { name, value } = event.target;
    setFormData((prevState) => ({ ...prevState, [name]: value }));
  };

  const closeForm = () => {
    setFormData(FORM_INITIAL);
  };

  const onSubmit = () => {
    dispatch({
      type: "auth/login",
      payload: {
        data: formData,
        users,
      },
    });

    closeForm();
  };

  return (
    <div
      style={{
        width: "100%",
        height: "100vh",
        margin: "auto",
        display: "grid",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#E1F0C7",
      }}
    >
      <div className="login-form">
        <h2>Login</h2>
        <div style={{ marginBottom: 10 }}>
          <p style={{ marginBottom: 0 }}>default username: tester</p>
          <span>default password: rahasia</span>
        </div>
        <TextField
          id="username"
          name="username"
          placeholder="Username"
          value={formData.username}
          onChange={onChange}
          className="mb-4"
        />
        <TextField
          id="password"
          name="password"
          placeholder="Password"
          value={formData.password}
          onChange={onChange}
          type="password"
          className="mb-4"
        />
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Link to={"/register"}>Daftar</Link>
          <Link to={"/forgot-password"}>Lupa password</Link>
        </div>

        <button type="button" className="submit-btn" onClick={onSubmit}>
          Login
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = ({ reducerAuth }: any) => {
  return { ...reducerAuth };
};

export const Login = connect(mapStateToProps)(_Login);
