import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Icon } from "react-materialize";
import styles from "./style.module.css";

const _Header = ({ dispatch }: any) => {
  const doLogout = () => {
    dispatch({
      type: "auth/logout",
    });
  };

  return (
    <div className={styles.headerLayout}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Link className={styles.link} to={"/"}>
          <Icon style={{ fontSize: 20, marginRight: 20 }}>home</Icon>
          To-do List
        </Link>
        <button type="button" className={styles.logout} onClick={doLogout}>
          Logout
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = ({ reducerAuth }: any) => {
  return { ...reducerAuth };
};

export const Header = connect(mapStateToProps)(_Header);
