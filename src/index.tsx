/* eslint-disable no-undef */
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
// import App from './App';
// import reportWebVitals from './reportWebVitals';
// import reducers from "./redux/reducers";
import sagas from "./redux/sagas";
import createSagaMiddleware from "@redux-saga/core";
import { Provider } from "react-redux";
// import { applyMiddleware, compose } from 'redux';
import { configureStore } from "@reduxjs/toolkit";
import { Routers } from "./utils/routers";
import { combineReducers } from "redux";
import { reducerAuth } from "./redux/auth/reducers";

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];
const rootReducer = combineReducers({
  reducerAuth,
});

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(...middlewares),
});
sagaMiddleware.run(sagas);

const users = [
  {
    username: "tester",
    password: "rahasia",
  },
];

localStorage.setItem("users", JSON.stringify(users));

ReactDOM.render(
  <Provider store={store}>
    <Routers />
  </Provider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
export { store };
