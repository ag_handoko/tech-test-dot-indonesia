import { combineReducers } from "redux";
import { reducerAuth } from "./auth/reducers";

export default function Reducer(): any {
  combineReducers({
    reducerAuth,
  });
}
