type props = {
  set_state: any;
  get_state: any;
  login: any;
  register: any;
  logout: any;
  update_password: any;
};

export const actions: props = {
  set_state: "auth/set_state",
  get_state: "auth/get_state",
  login: "auth/login",
  register: "auth/register",
  logout: "auth/logout",
  update_password: "auth/update_password",
};
