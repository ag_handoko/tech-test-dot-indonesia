import { actions } from "./actions";

const initialState = {
  loading: false,
};

export const reducerAuth = (state = initialState, action: any) => {
  switch (action.type) {
    case actions.set_state:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
