/* eslint-disable no-unused-vars */
import { all, put, takeEvery } from "redux-saga/effects";

import { actions } from "./actions";

function load(payload: boolean) {
  return setState({ loading: payload });
}

function setState(payload: { loading: boolean; users?: any[] }) {
  return put({
    type: "auth/set_state",
    payload,
  });
}

export function* getUsers() {
  const dataUser = JSON.parse(localStorage.getItem("users"));

  yield setState({
    loading: false,
    users: dataUser ?? [],
  });
}

export function* register({ payload }) {
  yield load(true);
  const { data, users } = payload;
  const findUser =
    users?.filter((val) => val.username === data.username)[0] || null;
  const newData = users !== undefined ? [...users] : [];
  newData.push(data);

  if (findUser) {
    yield load(false);
    alert("User sudah terdaftar");
  } else {
    yield load(false);

    localStorage.setItem("users", JSON.stringify(newData));
    yield put({
      type: "auth/get_state",
    });

    alert("Registrasi berhasil, silahkan login");
  }
}

export function* updatePassword({ payload }) {
  yield load(true);
  const { data, users } = payload;
  const findUser =
    users.filter((val) => val.username === data.username)[0] || null;
  const newData = JSON.parse(localStorage.getItem("users"));
  const index = newData.findIndex((val) => val.username === findUser.username);
  newData[index].password = data.password;
  console.log(newData);

  if (findUser) {
    yield load(false);

    localStorage.setItem("users", JSON.stringify(newData));
    yield put({
      type: "auth/get_state",
    });

    alert("Password berhasil dirubah");
    // yield setState({
    //   loading: false,
    //   users: newData,
    // });
  } else {
    yield load(false);
    alert("User tidak ditemukan");
  }
}

export function* login({ payload }) {
  yield load(true);
  const { data, users } = payload;
  const findUser =
    users.filter((val) => val.password === data.password)[0] || null;

  if (findUser) {
    yield setState({
      loading: false,
    });
    const token =
      Math.round(new Date().getTime() / 1000) +
      Math.random().toString(36).substring(2, 15) +
      Math.random().toString(36).substring(2, 15);

    localStorage.setItem("token", token);

    alert("Login berhasil");
  } else {
    yield load(false);
    alert("Password atau username tidak sesuai");
  }
}

export function* logout() {
  yield load(true);

  yield setState({
    loading: false,
  });

  localStorage.removeItem("token");
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.login, login),
    takeEvery(actions.logout, logout),
    takeEvery(actions.register, register),
    takeEvery(actions.get_state, getUsers),
    takeEvery(actions.update_password, updatePassword),
  ]);
}
