import { all } from 'redux-saga/effects';
import sagaAuth from './auth/saga';

export default function* rootSaga() {
	yield all([sagaAuth()]);
}
